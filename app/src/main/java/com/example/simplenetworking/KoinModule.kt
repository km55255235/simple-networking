package com.example.simplenetworking

import android.app.Application
import com.example.simplenetworking.api.ApiClient
import com.example.simplenetworking.repository.SimpleRepository
import com.example.simplenetworking.view.viewmodel.SimpleViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object KoinModule {
    val Application.dataModule
        get() = module {
            //DATABASE

            //API
            single { ApiClient.instance }
            //REPOSITORY
            factory { SimpleRepository(get()) }
        }

    val Application.uiModule
        get() = module {
            viewModel { SimpleViewModel(get())}
        }
}