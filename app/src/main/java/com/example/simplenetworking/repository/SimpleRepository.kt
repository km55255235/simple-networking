package com.example.simplenetworking.repository

import com.example.simplenetworking.api.APIService

class SimpleRepository (private val apiService: APIService) {

    suspend fun getCategory() = apiService.getCategory2()
}