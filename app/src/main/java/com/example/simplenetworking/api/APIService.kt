package com.example.simplenetworking.api

import com.example.simplenetworking.model.CategoryMenu
import com.example.simplenetworking.model.OrderRequest
import com.example.simplenetworking.model.OrderResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/* TODO LIST 2: Buat Interface API. yang memanggil endpointnya
* */
interface APIService {
    //untuk mapping Endpoint API
    @GET("category-menu")
    fun getCategory(): Call<CategoryMenu>

    @GET("category-menu")
    suspend fun getCategory2(): CategoryMenu

    @POST("order-menu")
    @FormUrlEncoded
    fun postOrder(@Field("username") username: String): OrderResponse

    @POST("movie/{movie_id}/rating")
    fun addRating(
        @Path("movie_id") movieId: String,
        @Query("guest_session_id") guest_session_id: String,
        @Query("session_id") session_id: String,
        @Body orderRequest: OrderRequest
    ): OrderResponse
}