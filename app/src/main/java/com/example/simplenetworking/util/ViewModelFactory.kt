package com.example.simplenetworking.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.simplenetworking.view.viewmodel.SimpleViewModel
import com.example.simplenetworking.api.APIService
import com.example.simplenetworking.repository.SimpleRepository

class SimpleViewModelFactory(val apiService: APIService): ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SimpleViewModel::class.java)) {
            return SimpleViewModel(SimpleRepository(apiService)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}