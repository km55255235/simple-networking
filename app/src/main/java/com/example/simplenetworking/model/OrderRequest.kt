package com.example.simplenetworking.model

data class OrderRequest(
    val orders: List<Order>,
    val total: Int,
    val username: String
)