package com.example.simplenetworking.view.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.simplenetworking.databinding.ActivityLoginBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        auth = Firebase.auth

        binding.btnAction.setOnClickListener {
            throw RuntimeException("Test Crash") // Force a crash
        }
    }

    fun register(email: String, password: String){
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful){
                    //Maka munculkan toast success
                    //insert database?
                    startActivity(Intent(this, MainActivity::class.java))
                    Toast.makeText(this, "Registrasi Berhasil", Toast.LENGTH_SHORT).show()
                    //insert firestore / DB Lokal / firebase realtime
                }else{
                    //Maka munculkan toast gagal
                    Toast.makeText(this, "Registrasi Gagal", Toast.LENGTH_SHORT).show()
                }
            }
    }

    fun login(email: String, password: String){
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful){
                    //Maka berhasil login
                    startActivity(Intent(this, MainActivity::class.java))
                    Toast.makeText(this, "Login Berhasil", Toast.LENGTH_SHORT).show()
                }else{
                    //User belum terdaftar
                    register(email, password)
                    Toast.makeText(this, "Login Gagal, Menuju Register", Toast.LENGTH_SHORT).show()
                }
            }
    }
    
}