package com.example.simplenetworking.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.example.simplenetworking.api.ApiClient
import com.example.simplenetworking.databinding.ActivityMainBinding
import com.example.simplenetworking.model.CategoryMenu
import com.example.simplenetworking.util.SimpleViewModelFactory
import com.example.simplenetworking.util.Status
import com.example.simplenetworking.view.viewmodel.SimpleViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.getViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: SimpleViewModel by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //fetchData()
        fetchDataCoroutines()
        //fetchUser()
    }

    //TODO LIST 4: FETCH DATA
    fun fetchData() {
        ApiClient.instance.getCategory()
            .enqueue(object : Callback<CategoryMenu> {
                override fun onResponse(
                    call: Call<CategoryMenu>,
                    response: Response<CategoryMenu>
                ) {
                    //Show Loading Indicator
                    val body = response.body()
                    Log.e("SimpleNetworking", Gson().toJson(body))
                    body?.let {
                        val data = body.data
                        val status = if (body.status != null) true else false
                        if (status) {
                            if (!data.isNullOrEmpty()) {
                                binding.progressBar.isVisible = false
                                //Mapping ke recyclerview
                            } else {
                                //???
                            }
                        } else {
                            //Jika kode != 200 handle disini
                        }
                    }
                    /*
                    * Jika response = 200 -> Mapping recycler view
                    * Jika response != 200 -> ???
                    * Jika response data is empty -> ???
                    * */
                }

                override fun onFailure(call: Call<CategoryMenu>, t: Throwable) {
                    Log.e("SimpleNetworking", t.message.toString())
                    /*
                    * response code != 200
                    * misal = 500 / 501
                    * Bisa di custom? jawabannya bisa
                    * */
                }

            })
    }

    fun fetchDataCoroutines() {
        viewModel.getAllCategory().observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.e("SimpleNetworking", Gson().toJson(it.data))
                    binding.progressBar.isVisible = false
                    val imageUrl = it.data?.data?.get(0)?.imageUrl
                    //Hide progressbar
                    //Mapping ke recyclerview
                }

                Status.ERROR -> {
                    Log.e("SimpleNetworking", it.message.toString())
                    //Hide progressbar
                    //Munculin Toast
                }

                Status.LOADING -> {
                    //Munculin progressbar
                }
            }
        }
    }

    fun fetchUser() {
        val user = Firebase.auth.currentUser
        user?.let {
            Log.e("SimpleNetworking", Gson().toJson(it))
            // Name, email address, and profile photo Url
            val name = it.displayName
            val email = it.email
            val photoUrl = it.zza()

            // Check if user's email is verified
            val emailVerified = it.isEmailVerified

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getIdToken() instead.
            val uid = it.uid
        }
    }
}