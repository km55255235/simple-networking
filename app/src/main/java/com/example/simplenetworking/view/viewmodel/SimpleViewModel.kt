package com.example.simplenetworking.view.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.simplenetworking.api.APIService
import com.example.simplenetworking.model.CategoryMenu
import com.example.simplenetworking.repository.SimpleRepository
import com.example.simplenetworking.util.Resource
import kotlinx.coroutines.Dispatchers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SimpleViewModel(private val repository: SimpleRepository) : ViewModel() {
    fun getAllCategory() = liveData(Dispatchers.IO) {
        try {
            emit(Resource.success(data = repository.getCategory()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    /*fun getAllCategory2() {
        apiService.getCategory().enqueue(object : Callback<CategoryMenu>{
            override fun onResponse(call: Call<CategoryMenu>, response: Response<CategoryMenu>) {
                //livedata.postValue()
            }

            override fun onFailure(call: Call<CategoryMenu>, t: Throwable) {
                //livedata.postValue()
            }
        })
    }*/
}